# butwhoami-rust

Various forms of identifying your machine.

## Usage

Assumes you have `cargo` installed.

```bash
$ cargo build --release
$ mv target/release/butwhoami /usr/bin/butwhoami
```

### Current username

Shows the output of `whoami`, the current user.

```bash
$ butwhoami user
andrew
```

### Current hostname

Shows the output of `hostname`, the current host.

```bash
$ butwhoami hostname
<hostname>
```

### Current IP address

Shows the response from `https://icanhazip.com`, if available.

```bash
$ butwhoami public_ip
55.0.0.55
```
