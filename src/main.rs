use clap::Parser;

mod methods;

#[derive(Parser)]
struct Cli {
    // The method used to determine "who am I?"
    method: String,
}

fn main() {
    let args = Cli::parse();
    // String::from("foo").as_ref()
    methods::apply_method(args.method.as_ref());
}
