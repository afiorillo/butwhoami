use std::process::Command;

/// Returns the output of calling `hostname` as a child of the current process.
/// It should return the hostname of the machine (as seen by that process).
pub fn whoiam() -> String {
    // calls $whoami
    let output = Command::new("hostname")
        .output()
        .expect("failed to execute process");

    // removes the trailing newline on Linux et al
    let stdout = String::from_utf8(output.stdout).expect("failed to parse process output");
    String::from(stdout.trim())
}

#[cfg(test)]
mod tests {
    use super::*;
    use whoami;

    #[test]
    fn matches_this_machine() {
        assert_eq!(whoiam(), whoami::hostname());
    }
}
