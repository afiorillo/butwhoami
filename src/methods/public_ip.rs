use reqwest;

/// Returns the ipv4 address of the current machine, as seen by an external host.
/// If connected to the internet, calls another server to ask for the current IP.
pub fn whoiam() -> String {
    // uses icanhazip.com
    let output = reqwest::blocking::get("https://icanhazip.com")
        .expect("could not get IP")
        .text()
        .expect("could not parse body");

    // removes the trailing newline on Linux et al
    String::from(output.trim())
}
