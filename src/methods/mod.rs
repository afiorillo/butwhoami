pub mod hostname;
pub mod public_ip;
pub mod user;
/// Methods are the collection of each way to ask "butwhoami"
/// Each file in this directory should map to an implementation of the ask.
/// For example, `user.rs` should contain the `butwhoami user` call.
///
/// Each module ought to implement a `whoami` function that prints the output.

pub fn apply_method(method: &str) {
    let result;

    match method {
        "user" => result = user::whoiam(),
        "hostname" => result = hostname::whoiam(),
        "public_ip" => result = public_ip::whoiam(),
        _ => panic!("unrecognized!"),
    }

    println!("{}", result)
}
